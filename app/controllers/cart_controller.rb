class CartController < ApplicationController
  def index
    @presets = Preset.all.first(5)
    @phones = Phone.all.first(3)
  end
end
