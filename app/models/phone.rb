class Phone < ActiveRecord::Base
  def name
    self.brand + " " + self.model
  end

  def price
    self.price_in_cents / 100
  end
end
