class Preset < ActiveRecord::Base
  def price
    self.price_in_cents / 100
  end

  def image
    self.image_url
  end
end
