class AddImageToPresets < ActiveRecord::Migration
  def change
    add_column :presets, :image_path, :string
  end
end
