class CreatePresets < ActiveRecord::Migration
  def change
    create_table :presets do |t|
      t.integer :people
      t.integer :price_in_cents

      t.timestamps
    end
  end
end
