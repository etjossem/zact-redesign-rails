class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.string :brand
      t.string :model
      t.string :slug
      t.integer :regular_price_in_cents
      t.integer :price_in_cents
      t.string :image_url

      t.timestamps
    end
  end
end
