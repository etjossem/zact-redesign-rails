# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Generate features (simple flags for turning functionality on and off)
##
features = [
  ["bml", true]
]
features.each do |feat|
  Feature.create( name: feat[0], enabled: feat[1] )
end
puts "Created " + features.count.to_s + " features"

# Generate presets
##
presets = [
  [1, 21.10],
  [2, 31.74],
  [3, 49.40],
  [4, 84.28],
  [5, 99.46]
]
presets.each do |pr|
  Preset.create( people: pr[0], price_in_cents: pr[1] * 100 )
end
puts "Generated " + presets.count.to_s + " presets"
##

# Generate phones
##
phones = [
  [
    "Samsung",
    "Galaxy SIII",
    "s3",
    599.00,
    299.00,
    "samsung-s3.png"
  ],[
    "Samsung",
    "Victory",
    "victory",
    399.00,
    259.00,
    "samsung-victory.png"
  ],[
    "LG",
    "Viper 4G LTE",
    "viper",
    299.00,
    199.00,
    "lg-viper.png"
  ]
]
phones.each do |ph|
  Phone.create( brand: ph[0], model: ph[1], slug: ph[2], regular_price_in_cents: ph[3] * 100, price_in_cents: ph[4] * 100, image_url: ph[5] )
end
puts "Generated " + phones.count.to_s + " phones"
##